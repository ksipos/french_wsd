import sys
from collections import Counter
from os import listdir
import csv
import string

import unicodedata
from unidecode import unidecode
import polyglot
from polyglot.text import Text, Word
from random import shuffle
import re

def stopword(term, pos):
    return pos.lower() not in ["verb", "noun", "adj"]

def remove_punc(text):
    text = text.replace("(", "")
    text = text.replace(")", "")
    text = text.replace("*", "")
    text = text.replace("^", "")
    text = text.replace("~", "")
    text = text.replace("@", "")
    text = text.replace("`", "")
    text = text.replace("''", "")
    text = text.replace("[", "")
    text = text.replace("]", "")
    text = text.replace("{", "")
    text = text.replace("}", "")

    return text

def remove_numbers(text):
    text = re.sub("[^\s^\n]+\.(php|html|dhtml|css|js|jsp)", "<URL>", text)
    text = re.sub("\\b\\d+\\b", "<NUM>", text)
    orig = text
    text = re.sub(r"\s+([^\s]+)(\s+\1)+\s*", r" \1 ", text)
    return text
    
def generate(prefix, documents):
    doc_id = -1
#    for document in documents:
#        doc_id += 1
#        with open(prefix + "/documents/doc" + str(doc_id) + ".txt", "w") as f:
#            for sentence in document:
#                f.write(" ".join(sentence[1]) + "\n")


    with open(prefix + "/classification_sentences.txt", "w") as f:
        for document in documents:
            sentences = []
            for line in document:
                sense_name = line[0]
                sentence = remove_punc(" ".join(line[1]))
                sentence = remove_numbers(sentence)
#                print(sentence)
                sentences += [[sense_name, s] for s in sentence.split(".") if s != '']
            for line in sentences:
                sense_name = ".".join("/".join(line[0].lower().split("/")[1:]).split(".")[:-1])
                sentence = line[1].lower()
                term = sentence
                #term = term.replace(">", "right_arrow")
                #term = term.replace("<", "left_arrow")
                #term = term.replace("'", "single_quote")
                #term = term.replace('"', "double_quote")
                #term = term.replace("&", "ampersand")
                term = term.strip()
                if len(term.split()) < 3 or len(term.split()) > 200:
                    continue
                f.write("\"" + term + "\" " + sense_name + "\n")
                f.flush()


names = ["tokenized/" + name for name in listdir("tokenized")]

sentences = []
for name in names:
    with open(name, "r") as f:
        r = csv.reader(f, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL)
        for line in r:
            if len(line) == 1 and line[0] == '':
                continue
            sentences.append([name, line])
shuffle(sentences)

n = 1000
documents = [sentences[i:i + n] for i in range(0, len(sentences), n)] #n : number of sentences per chunk to split the list


split_pos = round(len(documents) * 0.9)
training = documents[:split_pos]
test = documents[split_pos+1:]

generate('training', training)
generate('test', test)

