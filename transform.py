import sys
from collections import Counter
from os import listdir
import csv
import string

import unicodedata
from unidecode import unidecode
import polyglot
from polyglot.text import Text, Word
from random import shuffle
import re

def stopword(term, pos):
    return pos.lower() not in ["verb", "noun", "adj"]

def remove_punc(text):
    text = text.replace("(", "")
    text = text.replace(")", "")
    text = text.replace("*", "")
    text = text.replace("^", "")
    text = text.replace("~", "")
    text = text.replace("@", "")
    text = text.replace("`", "")
    text = text.replace("''", "")
    text = text.replace("[", "")
    text = text.replace("]", "")
    text = text.replace("{", "")
    text = text.replace("}", "")

    return text

def remove_numbers(text):
    text = re.sub("[^\s^\n]+\.(php|html|dhtml|css|js|jsp)", "<URL>", text)
    text = re.sub("\\b\\d+\\b", "<NUM>", text)
    orig = text
    text = re.sub(r"\s+([^\s]+)(\s+\1)+\s*", r" \1 ", text)
    return text
    
def generate(prefix, documents):
    doc_id = -1
    for document in documents:
        doc_id += 1
        with open(prefix + "/documents/doc" + str(doc_id) + ".txt", "w") as f:
            for sentence in document:
                f.write(" ".join(sentence[1]) + "\n")


    senses = open(prefix + "/yahoo.gold.key.txt", "w")
    with open(prefix + "/yahoo.data.xml", "w") as f:
        f.write('<?xml version="1.0" encoding="UTF-8" ?>\n')
        f.write('<corpus lang="fr" source="yahoo">\n')
        text_id = -1
        for document in documents:
            text_id += 1
            f.write('<text id="d' + str(text_id) + '">\n')
            sent_id = -1
            sentences = []
            for line in document:
                sense_name = line[0]
                sentence = remove_punc(" ".join(line[1]))
                sentence = remove_numbers(sentence)
#                print(sentence)
                sentences += [[sense_name, s] for s in sentence.split(".") if s != '']
            for line in sentences:
                if sent_id == 492744 - 5:
                    print(line)
                    import sys
                    sys.exit(20)
                sense_name = line[0]
                sentence = line[1]
                sent_id += 1
                f.write('<sentence id="d' + str(text_id) +'.s' +  str(sent_id) + '">\n')
                term_id = -1
                plg_text = Text(sentence)
                plg_text.language = 'fr'
                for term, pos in plg_text.pos_tags:
                    term = term.replace(">", "right_arrow")
                    term = term.replace("<", "left_arrow")
                    term = term.replace("'", "single_quote")
                    term = term.replace('"', "double_quote")
                    term = term.replace("&", "ampersand")
                    term = term.strip()
                    if len(term) < 1:
                        term = "empty"

                    term_id += 1
                    lemma = term # Replace with proper lemmatizer
                    complete_text = 'lemma="' + lemma + '" pos="' + pos + '">' + term
                    if stopword(term, pos):
                        f.write('<wf ' + complete_text + '</wf>\n')
                    else:
                        f.write('<instance id="d' + str(text_id) + '.s' + str(sent_id) + '.t' + str(term_id) + '" ' + complete_text  + '</instance>\n')
                        senses.write('d' + str(text_id) + '.s' + str(sent_id) + '.t' + str(term_id) + ' ' + sense_name.lower() + "\n")
                        senses.flush()
                f.write('</sentence>\n')
            f.write('</text>\n')

    #        sent = sent.replace("\n", "")
        #    for word in sent.split():


        f.write("</corpus>")
    senses.close()

names = ["tokenized/" + name for name in listdir("tokenized")]

sentences = []
for name in names:
    with open(name, "r") as f:
        r = csv.reader(f, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL)
        for line in r:
            if len(line) == 1 and line[0] == '':
                continue
            sentences.append([name, line])
shuffle(sentences)

n = 1000
documents = [sentences[i:i + n] for i in range(0, len(sentences), n)] #n : number of sentences per chunk to split the list


split_pos = round(len(documents) * 0.9)
training = documents[:split_pos]
test = documents[split_pos+1:]

generate('training', training)
generate('test', test)

