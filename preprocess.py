from os import listdir

import nltk
from nltk.tokenize import word_tokenize
import re
from collections import Counter
import csv
from os import walk
from os.path import join

url_regex = re.compile('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', re.IGNORECASE)

print("Starting")

#directories = {directory: listdir("./data/" + directory) for directory in listdir("./data")}

filenames = []
for path, subdirs, files in walk("data"):
    for name in files:
        filenames.append(join(path, name))

collapse = ["Économie_locale", "Restaurants", "Voyage"]

content = {}
for fil in filenames:
    if fil.split("/")[-3] in collapse:
        name = fil.split("/")[-3]
    else:
        name = fil.split("/")[-2]
    if "questions.txt" in fil:
        with open(fil, "r") as f:
            content[name] = [", ".join(line.split(", ")[3:]) for line in f.readlines()]
        continue

    with open(fil, 'r') as f:
        content[name] = content.get(name, []) + [line.replace("\n", "") for line in f.readlines()]
words = []
for category, sentences in content.items():
    tokenized = []
    for sentence in sentences:
        sentence = re.sub(url_regex, "", sentence.lower())
        sentence = word_tokenize(sentence, language='french')
        if len(sentence) > 0:
            tokenized.append(sentence)
            words += sentence
    with open("tokenized/" + category + ".csv", "w") as f:
        cswr = csv.writer(f, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL)
        for to in tokenized:
            cswr.writerow(to)
counted = Counter(words)
