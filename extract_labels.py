import sys
from collections import Counter
from os import listdir
import csv
import string
import re
import unicodedata
from unidecode import unidecode
import emoji
import polyglot
from polyglot.text import Text, Word

def remove_emoji(text):
    return emoji.get_emoji_regexp().sub(u'', text)


def preprocess(line):
    sentence = " ".join(line).strip().replace("\n", "")
    sentence = "".join([char for char in sentence if char == "'" or char not in punc])
    sentence = re.sub(r'[0-9]+', '<NUM>', sentence) # Replace numbers
    sentence = re.sub(r'([a-z])\1+', r'\1', sentence)
    sentence = re.sub(r"\s'|'\s|'\s'", "", sentence)
    sentence = re.sub(r'\s[a-zA-Z]\s', "", sentence)
    if len(sentence) < 1:
        return ""
    sentence = sentence.strip()
    if len(sentence.split()) < 2:
        return ""
    sentence = remove_emoji(sentence)
#    if 'fr' != Text(sentence).language.code and Text(sentence).language.confidence > 98:
#        print(sentence, Text(sentence).language)
#        return ""
    return sentence

names = ["tokenized/" + name for name in listdir("tokenized")]
punc = set(string.punctuation)
tokens = []
sentences = []
for name in names:
    with open(name, "r") as f:
        r = csv.reader(f, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL)
        for line in r:
            sentence = preprocess(line)
            if len(sentence) < 1:
                continue
            tokens += sentence.split()
            sentences.append(sentence)
#            line =  ["".join(e for e in l if e in [" ", "?", ".", '-', ',', '_'] or e.isalnum()) for l in line if l not in punc]
#            tokens += line
#            sentences.append(" ".join(line))
with open("sentences.txt", "w") as f:
    for s in sentences:
        f.write(s + "\n")

with open("stopwords.txt", "r") as f:
    stopwords = [line.replace("\n", "") for line in f]
#stopwords = {element[0]: element[1] for element in Counter(tokens).most_common(50)}
#removed_words = list(punc) + list(stopwords.keys())


word_cat = {}

for name in names:
    with open(name, "r") as f:
        r = csv.reader(f, delimiter=",", quotechar="|", quoting=csv.QUOTE_MINIMAL)
        cat_tokens = []
        for line in r:
            sentence = preprocess(line)
            if len(sentence) < 1:
                continue
#            cat_tokens += ["".join(e for e in l if e in [" ", "?", ".", '-', ',', '_'] or e.isalnum()) for l in line if l not in removed_words]
            cat_tokens += [word for word in sentence.split() if word not in stopwords]
        for tok, num in Counter(cat_tokens).items():
            tmp = word_cat.get(tok, {})
            tmp["/".join(name.split("/")[1:])] = num
            word_cat[tok] = tmp

with open("vocabulary.txt", "w") as f:
    for element in Counter(tokens).most_common():
        token = element[0]
        if token in stopwords:
            continue
        if element[1] >= 5:
            f.write(token + " \t" + str(element[1]) + "\t" + "\t".join([k + "_" + str(v) for k, v in sorted(word_cat[token].items(), reverse=True,  key=lambda x: x[1])]) + "\n\n")

