 Quand le ciel bas et lourd pèse comme un couvercle 

Quand le ciel bas et lourd pèse comme un couvercle 
Sur l'esprit gémissant en proie aux longs ennuis, 
Et que de l'horizon embrassant tout le cercle 
Il nous verse un jour noir plus triste que les nuits ; 

Quand la terre est changée en un cachot humide, 
Où l'Espérance, comme une chauve-souris, 
S'en va battant les murs de son aile timide 
Et se cognant la tête à des plafonds pourris ; 

Quand la pluie étalant ses immenses traînées 
D'une vaste prison imite les barreaux, 
Et qu'un peuple muet d'infâmes araignées 
Vient tendre ses filets au fond de nos cerveaux, 

Des cloches tout à coup sautent avec furie 
Et lancent vers le ciel un affreux hurlement, 
Ainsi que des esprits errants et sans patrie 
Qui se mettent à geindre opiniâtrement. 

- Et de longs corbillards, sans tambours ni musique, 
Défilent lentement dans mon âme ; l'Espoir, 
Vaincu, pleure, et l'Angoisse atroce, despotique, 
Sur mon crâne incliné plante son drapeau noir. 

Charles BAUDELAIRE   (1821-1867)
 Ce que souffle le vent à l 'oreille des nuages et qui leur fait verser des larmes. 
---------------------------------------... 

"Rayures d'eau, longues feuilles couleur de brique,  
Par mes plaines d'éternité comme il en tombe !  
Et de la pluie et de la pluie - et la réplique  
D'un gros vent boursouflé qui gonfle et qui se bombe  
Et qui tombe, rayé de pluie en de la pluie. 

- Il fait novembre en mon âme - 
Feuilles couleur de ma douleur, comme il en tombe ! 

Par mes plaines d'éternité, la pluie  
Goutte à goutte, depuis quel temps, s'ennuie, 
- Il fait novembre en mon âme - 
Et c'est le vent du Nord qui clame 
Comme une bête dans mon âme. 

Feuilles couleur de lie et de douleur,  
Par mes plaines et mes plaines comme il en tombe ;  
Feuilles couleur de mes douleurs et de mes pleurs,  
Comme il en tombe sur mon coeur ! 

Avec des loques de nuages, 
Sur son pauvre oeil d'aveugle  
S'est enfoncé, dans l'ouragan qui meugle,  
Le vieux soleil aveugle. 

- Il fait novembre en mon âme - 
...." 


Émile Verhaeren 
 "La Danse de la pluie vient de la culture de mon peuple. Nous avons recommencé à la danser il y a 15 ou 16 ans. Elle a été réintroduite dans cette région. Avant cela, elle n'avait pas été effectuée depuis 100 ans. Lorsqu'elle a été interdite, lorsqu'il était illégal d'organiser des cérémonies, un certain nombre de gens l'ont emmenée dans l'ouest. Elle est restée là-bas. Avant la venue de l'homme blanc, nous avions nos baluchons sacrés contenant les pipes. Dans l'ancien temps, la Danse de la pluie était connue sous le nom de Danse de la soif, parce que quand ils soufflaient dans le sifflet en os d'aigle jusqu'à ce qu'il les assèche. Elle ouvre l'esprit sur [l'amour et la bonté]. Quand cette cérémonie était illégale, mon peuple s'enfonçait dans les bois pour effectuer les cérémonies loin des yeux de l'église et du gouvernement. 

"Lors de la grande sécheresse dans les années 1920 et au début des années 1930, les fermiers se sont rassemblés et sont allés trouver le shérif pour lui dire qu'il fallait agir vis-à-vis de cette sécheresse. Ils ont demandé la permission d'aller voir les membres sacrés [chez les Anishinaabes] pour leur demander de faire tout ce qu'ils pouvaient pour qu'il pleuve. Tout ce que ces derniers eurent à faire fut d'allumer la pipe et prier. 
https://www.youtube.com/watch?v=o5Y8HkkEKOk
 Bonjour .... 
La pluie me plait.. Peut être parce que j'habite dans le sud et qu'elle est rare! C'est un bonheur d'aller au lit le soir quand on l'entend au dehors tomber, c'est très réconfortant.. Quand j'entends le mot "pluie", de suite je pense à ce poème de Paul Verlaine appris au lycée.. Là, elle n'évoque pas la joie ou le réconfort mais au contraire la tristesse et l'ennui..  
Et merci d'évoquer la pluie en pleine canicule, là maintenant tout de suite, elle me fait carrément rêver .. Juste pour sa fraicheur! 

Il pleure dans mon coeur 

Il pleure dans mon coeur 
Comme il pleut sur la ville ; 
Quelle est cette langueur 
Qui pénètre mon coeur ? 

Ô bruit doux de la pluie 
Par terre et sur les toits ! 
Pour un coeur qui s’ennuie, 
Ô le chant de la pluie ! 

Il pleure sans raison 
Dans ce coeur qui s’écoeure. 
Quoi ! nulle trahison ?… 
Ce deuil est sans raison. 

C’est bien la pire peine 
De ne savoir pourquoi 
Sans amour et sans haine 
Mon coeur a tant de peine. 

Paul Verlaine 

https://www.youtube.com/watch?v=YluAf2T6... 

Bonne fin d'après-midi, bye ☺
 Bonsoir, 
Un poète persan a célébré la pluie : Goltchin Guilâni : 

La pluie 

De nouveau, la pluie  
Chantant une mélodie, 
Versant une myriade de joyaux, 
Tombe sur le toit de la maison. 

Le jour pluvieux me rappelle 
La promenade d’une journée lointaine, 
Douce et agréable, 
A travers les forêts du Guilân : 

J’étais alors un gamin de dix ans 
Heureux et content 
Souple et fragile 
Fringant et agile 
Avec mes petits pieds enfantins 
Je courais comme une gazelle 
Je sautais le ruisseau 
Je m’éloignais de la maison. 

L’oiseau, 
Le vent soufflant me racontaient, 
Des histoires secrètes 
Des mystères de la vie. 

La foudre comme une épée tranchante 
Coupait les nuages 
Rugissant, le tonnerre aliéné 
Battait les nuages 

La forêt 
Fuyant le vent 
Tournait et tournait  
Comme la mer 
Partout tombaient 
Les perles rondes de pluie 

Le gazon au pied de l’arbre 
S’est transformé petit à petit 
En une mer 
Dans cette mer liquide, 
La forêt, à l’inverse 
était apparente. 

Comme elle était savoureuse, la pluie 
Comme elle était belle 
Dans cette chute de perles 
J’entendais 
Des mystères éternels 
Des mots célestes 
Oh mon enfant ! écoute-moi ! 
Du regard de l’homme mûr du demain 
La vie 
Soit en rose, soit en noir 
Est belle, est belle, est belle.
 La pluie lave l'ennuie, 
inspire les créatifs. 
Efface ce qui est inscrit, 
les insultes, les injures. 
Laisse la place aux jolis mots. 
La douceur d'un baiser sous un porche. 
Deux personnes s'enlacent, 
sous la pluie, écoute son chant, 
riant de l'incongruité. 
.
 slt camarade j'aime ce poéme de Baudelaire 
Baudelaire est à la pluie ce que Qu'Enaudi est au soleil 
j'entends bien ce poéme avec cette musique là 

https://www.youtube.com/watch?v=2d6HmH-j...
 une libération
 L'amour sous influence de la pollution atmosphérique.
 La pluie est naturelle
 Il en fumait du bien mauvais !