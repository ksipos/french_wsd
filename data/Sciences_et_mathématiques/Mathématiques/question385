 Résolution "physique", une fois n'est pas coutume, d'une équation mathématique : 

Ce que vous décrivez, c'est la route la plus rapide dans un milieu dans lequelle la vitesse est 1/(y+1). S'il s'agissait de lumière, il s'agirait donc de la route la plus rapide dans un milieu dans lequel l'indice de réfraction est proportionnel à y+1. 

Le ratio des indices de réfraction est inverse du ratio des vitesses ; dans la vraie vie, on désigne 1 l'indice de réfraction du vide, cad quand la vitesse est égale à c. Mais on pourrait choisir n'importe quelle base, puisque ce sont les ratios qui comptent. 
Donc, ici, prenons comme base c=1 
Et donc, ça revient à chercher le chemin le plus court de la lumière dans un milieu dont l'indice de réfraction est y+1 

Or, selon le principe du plus court chemin de la lumière, il se trouve que pour aller d'un point A à un point B, la lumière passe par son chemin le plus rapide.  
On a donc deux visions d'un même problème, quand on essaye de comprendre ce qui se passe quand un rayon de lumière rentre dans un verre d'eau. 

Soit on peut dire : la lumière a été infléchie. En suivant la règle n1*sin(a1)=n2*sin(a2)  
(où a et b sont les angles du rayon de la lumière avec la normale à la surface entre les deux milieux, et n1 et n2 les indices de réfraction des milieux). 
Soit on peut dire : la lumière a choisi le plus court chemin pour aller de son point de départ à son point d'arrivée. 

Les deux sont équivalents. 

Et c'est ce qui va me permettre, grâce à cette analogie, de dire donc que votre problème est le même que celui qui consiste à calculer la trajectoire d'un rayon qui est infléchi entre chaque "couche" de y, par la règle n1sin(a1)=n2sin(a2) 

Ici, les couches sont infinitésimales.  
Ce que je vais chercher à déterminer c'est l'impact que ça a sur la courbe y=f(x) 

Entre deux points x, et x+dx, on a donc 
n(x)*sin(a(x)) = n(x+dx)*sin(a(x+dx)) 
sin(a(x)) c'est connu : c'est le sinus de la pente de la courbe. Ici, donc 1/racine(1+y'²) 
(Faites du pythagore s'il faut pour retrouver ça : quand x avance de 1, y avance de y', en moyenne) 
n(x) aussi : c'est y+1 
Donc on se retrouve avec une équation 
(y+1)/racine(1+y'²) = (y+y'dx+1)/racine(1+(y'+y"dx)²) 
(Puisque dx étant infinitésimal, y(x+dx)=y(x)+y'(x)*dx, et y'(x+dx)=y'(x)+y"(x)dx) 

Ce qui m'intéresse ici, c'est la valeur de y", quand dx tend vers 0. dx est infinitésimal ; ce qui est bien pratique, parce que ça m'autorise à faire des développements limités à chaque étape de la fabrication 

Par exemple, je constate que racine(1+(y'+y"dx)²)  
= racine(1+y'²+2*y'*y"*dx +o(dx)) [je néglige le terme y"²dx², puisque dx² est négligeable devant dx] 
 = racine(1+y'²)*racine(1+2*y'*y"*dx/(1+y'²... + o(dx))  [je factorise par 1+y'²] 
= racine(1+y'²)*(1+y'*y"*dx/(1+y'²)) + o(dx) [je remplace la 2e racine carrée racine(1+u) en 1+u/2] 

[Notez bien que mes "négligences" et "approximations" sont exactes. C'est pas de l'à peu près. Je n'introduis aucune erreur en les faisant, puisque dx est ifinitésimal. J'aurais pu d'ailleurs faire sauter les termes en "dx" aussi, et ça serait resté parfaitement exact. C'est juste que je sais où je veux en venir, et je sais que si je les vire, je vais me trouver avec un toto=toto qui me sert à rien. Alors qu'en les gardant, j'espère me retrouver avec un toto+f"*dx = toto+bidule*dx, qui me permettra de dire f"=bidule.] 

Ce qui me permet de récrire mon équation 
y+1 = (y+y'dx+1)/(1+y'*y"*dx/(1+y'²)) + o(dx) 

Ou encore 1+(y"*y'*dx)/(1+y'²) = (1+y+y'dx)/(1+y) 

Donc (1+y)(1+y'²)+(1+y)(y"y'dx) = (1+y'²)(1+y+y'dx) 

Quand on développe, tous les termes sans "dx" se simplifient (Comme je le disais dans la note précédente^^ ce qui est heureux, je ne voulais pas qu'il me reste un "dx" qui est infinitésimal ; le but est de trouver une valeur à y" ; comme il ne me reste que des termes en dx, je vais pouvoir diviser par dx, cad dériver) 
Et il me reste :  
y"*y'*dx + y"*y'*y*dx = y'*dx + y'³*dx 
Donc  
y" = (1+y'²)/(1+y) 

Ce qui, fait une assez belle équation différentielle, compte tenu de la complexité des développements par lesquels on est passé. 

Cette équation est assez pénible à résoudre, comme beaucoup d'équations différentiels d'ordre 2 contenant du 1/(1+y) , il est plus facile d'exprimer x en fonction de y que l'inverse. 
Faisons ça. De toutes façons, ça vous donne la courbe tout aussi bien. 

L'équation est dans l'image  attachée.  
Avec A et B qui se déterminent en fonction des valeurs initiales. 
Ici ça donne A=1.052659690493734 et B=2.01413832 

La courbe attachée est la superposition dans le même fichier (les deux sont indiscernables) du résultat de l'équation, et d'un calcul numérique par dijkstra de la trajectoire optimale (cad en résolvant numériquement votre problème initial, non transformé en calcul de diffraction : on cherche la trajectoire optimale en calculant les longueurs de toutes les trajectoires possibles, dans un espace discrétisé de 100 millions de points). Le fait d'obtenir la même chose par deux démarches radicalement différentes valide donc le résultat. 
 Le chemin le plus court, c'est un arc de grand cercle de la sphère terrestre passant par A et B, pour la vitesse, il faut bien sûr tenir compte de la résistance de l'air, de la force et de l'orientation du vent, de la résistance du sol si on est sur terre ou de l'eau si l'on est sur mer.
 La question est correcte mais la résoudre...
 Ton énoncé est au moins incomplet.  
La nature du sol ? Elle doit imposer d'autres choses que ce qui semble être une vitesse initiale (disons V0) dont tu indiques ce qui ressemble au module (v qui serait égale à 1/2) sans les composantes (Vx et Vy) c'est-à-dire la direction ... et quelques autres bricoles.
 en avion
 Le chemin le plus court en géométrie est la ligne droite . 

.En géographie c 'est à vol d' oiseau ou à la nage . 

En politique c'est la corruption. 

En amour c'est le lit. 

. 

Ton probléme est incompréhensible . 

.de la façon donc tu le présentes. 

A refaire . 

Croquis bienvenus. 

A ______________ B 


.