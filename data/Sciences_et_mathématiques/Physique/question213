 Je ne connaissais pas la formule.  

Ce qu'on peut en dire c'est qu'elle n'est plus valable, puisque aujourd'hui les masses sont des variables. 

Du temps de Poincaré, cela avait un sens. Au final qu'était une masse ? Une force, à un coefficient multiplicatif près. Le ratio entre une force et l'accélération qu'elle produit. 

Beaucoup de gens croyaient la mesurer, la sentir, alors que dans les faits ce qu'ils savent mesurer, ce sont des forces et des accélérations.  
Donc, la formule provoquante avait un sens : rappeler que la masse était quelque chose d'assez intangible en réalité, qu'on croyait percevoir, mais en n'en percevant que les conséquences. Que ce que les gens appelaient "masse" était surtout cette conséquence.  

Une situation un peu étonnante, par exemple, qui justifie cette formule provoquante : alors qu'il n'existe pas de "massemètre" (tous ne sont que des accéléromètres, des newtonmètre, voire des baromètres), beaucoup de ces appareils affichent quand même leur résultat en kg ; et je ne parle pas seulement là du fait que votre pèse personne mesure des N pour afficher des kg (et cesserait donc de donner le bon affichage sur la Lune ou dans un ascenseur) ; je fais aussi allusion au fait que dans certains milieux scientifiques (l'ingéniérie pétrolière, par exemple), on exprime facilement des forces en kg, et des pressions en psi (livres par pouce carré, donc masse par unité de surface). C'est à dire qu'on trouve plus aisé d'utiliser une unité fausse (de masse) pour quantifier une mesure autre (de force ou de pression), et ce alors que cette unité de masse ne quantifie qu'un coefficient dans les calculs, et pas une réalité tangible, alors que l'unité qu'on choisit d'improprement remplacer par elle (newton, pascal), elle quantifiait quelquechose de tangible. 

Bref, j'approve la remarque. La masse n'est bien qu'un coefficient dans les calculs. Et il ne faut pas confondre ce coefficient, avec les vraies grandeurs, tangibles (pression, force, accélération) sur lesquels ce coefficient joue, et qu'on a tendance pourtant à trouver plus abstraits que la masse 

Mais malheureusement, même si je l'approuve, la formule n'est plus valable aujourd'hui. 


La version de remplacement qu'Einstein pourrait proposer c'est "La masse n'est que de l'énergie, sous un autre nom" 

EDIT: 
Pour répondre à la réponse, oui, en effet, Poincarré connaissait la relativité, puisqu'on accuse souvent Einstein d'en avoir récupéré à tort la paternité. 

Mais j'ai le sentiment que cette formule ne fait pas allusion à la relativité. Ou alors qu'il n'avait pas compris lui même cette implication de la relativité (la masse n'est pas une constante ; mais j'en doute, parce que vu la forme qu'il donnait à sa formule, elle semble plus focalisée sur la variabilité de la masse que E=mc². E=mc² et m=E/c² c'est la même chose. Mais l'un dit que l'énergie totale est constante, comme l'est la masse au repos. L'autre dit que la masse est variable, comme l'est l'énergie) 

Mais après tout, peut être que je me trompe, et que tout au contraire il faisait déjà allusion à E=mc² en disant cela. Et que la masse dont il parle alors (en la traitant de "coefficient") est la masse au repos (qui est constante, même d'un point de vue relativiste) 


"Je me demande s’il n’avait pas à l’esprit qu’on ne disposait à l’époque d’aucune autre grandeur physique mesurable qu’on pût attacher à la masse".  

Oui, exactement. C'est ce que je cherchais à dire avec ma remarque sur les gens qui mesurent des pressions en PSI, des forces en kg, etc. 
Ils le font parce qu'ils croient que la masse est plus tangibles que ces notions abstraites de pression et de forces. 
Alors qu'en réalité, c'est la masse qui est bien la donnée la plus intangibles parmi ces 3 (pressions, forces, masse). Qu'on est réduit à mesurer qu'en observant son effet sur d'autres grandeurs. 

Ça, je pense que c'est la source de la phrase. Qu'on la considère comme non relativiste. Ou comme relativiste (auquel cas le message devient en plus : et, pas de bol, ces relations qu'on croyait simple entre ces mesures tangibles et la masse le sont moins) 


Quant à la question : je ne suis pas assez calé en physique théorique pour connaître par coeur les découvertes en fonction de leurs date (et avec google ce serait un peu trop facile^^). 
Mais puisqu'on parle de Broglie/Planck, le point commun qui saut aux yeux, c'est l'équivalence "matière = onde" (pour Planck, ce qu'on appelle "matière" n'est que la manifestation de forces électromagnétique) 

Donc, j'imagine que ce que vous avez en tête est E=hν, ou un truc comme ça. Qui relie, indirectement, la masse (puisque E=mc² nous dit que c'est équivalent à de l'énergie) à une onde. 
 @Oyubir : 

La version de remplacement dont tu parles, m = E/c²,  était pourtant bien connue de Poincaré, à l’époque (1902, je crois) où il écrivait que les masses n’étaient autres que de commodes coefficients : C’est dire s’il avait bien conscience de leur caractère relatif et variable. Je me demande s’il n’avait pas à l’esprit qu’on ne disposait à l’époque d’aucune autre grandeur physique mesurable qu’on pût attacher à la masse. Et, il me semble qu’une des grandeurs mesurables en rapport avec la masse a pleinement été mise en lumière (aux sens propre et figuré) par Planck vers 1915. Laquelle ? (He hé ! ...). Vous voulez un autre indice ? De Broglie, en 1924... 

P.S. : Avec un peu de chance, on pourra progresser ici sur ce sujet jusqu'en 2017, grâce aux lumières des uns et des autres (le peuple, quoi, avec ses joies et ses peines !). 

---------------------------------------... 
De Poincaré : 

Pour un observateur superficiel, la vérité scientifique est hors des atteintes du doute ; la logique de la science est infaillible et, si les savants se trompent quelquefois, c’est pour en avoir méconnu les règles. Les vérités mathématiques dérivent d’un petit nombre de propositions évidentes par une chaîne de raisonnements impeccables ; elles s’imposent non seulement à nous, mais à la nature elle-même. Elles enchaînent pour ainsi dire le Créateur et lui permettent seulement de choisir entre quelques solutions relativement peu nombreuses. Il suffira alors de quelques expériences pour nous faire savoir quel choix il a fait. De chaque expérience, une foule de conséquences pourront sortir par une série de déductions mathématiques, et c’est ainsi que chacune d’elles nous fera connaître un coin de l’Univers. 

---------------------------------------... 

EDIT 1 

Oui, c'est en effet à "E = hν" que je pensais et, partant, à la physique quantique. 
Pour ne pas prolonger davantage cette question, on peut simplement rappeler qu'aujourd'hui la masse est associée au champ scalaire de Higgs, autrement dit, elle n'est pas intrinsèque aux particules. 
Merci à tous pour vos réponses.
 La physique ne se réduit pas aux mathématiques.
 On ne propose pas mieux, mais on propose différent.
 Encore un qui a beaucoup de considérations pour le pauvre peuple , rien à ajouter .