 Pour les systèmes planétaires, il y a trois mécanismes qui expliquent que les planètes se trouvent majoritairement dans le même plan. 

Le premier est la conservation du moment cinétique. C'est une grandeur physique qui lie vitesse, masse du mobile et distance au centre de gravité du système. Pour faire simple c'est ce qui permet à la patineuse ou à l'enfant sur une balançoire de centrifuger son cerveau : bras écartés = rotation lente, bras serrés près du corps = rotation rapide. 
Quand une étoile de forme son nuage de gaz primordial a nécessairement un léger mouvement de rotation (le contraire est presque impossible statistiquement ; ça vient en particulier qu'il a été en interaction avec d'autres nuages, étoiles et le champ de gravité de la galaxie). Pour que se forme l'étoile, il faut que le nuage se contracte, ce qui va augmenter la vitesse de rotation. Ainsi ce qui n'était que faiblement perceptible  va devenir plus marquer et un axe de rotation deviendra prééminent. 

Mais rien n'empêche encore les mouvements à être très éloignés d'un plan moyen. 

Dans un premier temps il y a un phénomène qui permet à la poussière (on en est encore à ce stade) de se condenser autour de ce plan moyen. C'est que justement un plan est un excellent radiateur et que son contenu se refroidit ainsi rapidement et perd de la vitesse, en particulier pour s'éloigner du plan. Au début on est plus près du ballon de rugby mais cela aide déjà à accélérer le refroidissement, et cela va d'autant plus vite que le disque s'aplatit. 

Enfin une fois que des corps un peu conséquents se sont formés, en particulier quelques planétoïdes, ceux-ci vont s'influencer gravitationnellement par un effet de rappel vers le plan de l'astre le plus influant (celui qui va avoir le plus gros moment cinétique justement). 
L'influence se fait par attraction directe (on imagine bien qu'en passant à proximité l'une de l'autre, les corps vont chercher à se rapprocher, donc leur plans orbitaux), mais aussi via l'étoile centrale. Cela est d'autant plus vrai que les corps sont massifs. Il s'y passe en fait exactement la même chose qu'avec la Lune : des effets de marées. Ici par exemple Jupiter soulève la surface solaire dans sa direction, ce qui modifie la gravité solaire et ses bourrelets attirent les planètes qui ne sont pas dans le plan vers ce plan. C'est ce même effet de rétro-action de marée qui à verrouillé la Lune face à la Terre lui donnant des périodes de révolution et de rotation similaires. 
En fait il y a même un second effet. Il s'agit du positionnement et/donc de la régularisation des périodes orbitales en concordance harmonique, ou du moins en résonance forte. Si tu calcules les rapports de période entre planètes et en particulier avec Jupiter, tu constateras que ceux-ci sont très proches de rapport simples (2/3, 1/2, etc.), preuve qu'il y a bien influence mutuelle et échange d'énergie pour s'aligner en résonance, ou anti-résonance. Il n'y a qu'un ou deux couples qui ne fonctionnent pas (Neptune et Pluton sont plutôt a considérer comme un seul astre en ce moment, ils n'ont pas encore finis de s'expliquer). 


Pour les galaxies on doit retrouver au départ un mécanisme similaire : refroidissement, donc condensation, donc rotation privilégiée autour d'un axe. C'est typiquement ce qu'on retrouve dans une galaxie spirale en particulier quand on considère les différents disque comme le disque de poussière plus refroidit donc condensé que le disque d'étoiles ou de nuages de gaz. 

Mais toutes les galaxies ne sont pas formées ainsi. L'autre grand groupe sont les galaxies elliptiques, très différentes : 
- par de forme spirale, 
- pas ou peu de gaz, 
- pas d'axe de rotation privilégié (d'où la forme ellipsoïde) 
Manifestement leur formation est très différente, et en fait leur localisation plus nombreuses près du centre des amas de galaxies donne une bonne indication. Vraisemblablement elles proviennent de la fusion de plusieurs (deux, plus ?) galaxies « normales » (aplaties donc spirales mais pas que) donc les moments cinétiques se sont mélangés de telle manière que les étoiles n'ont plus un axe de rotation privilégié dans la galaxie. C'est leur proximité dans les amas qui permet ces collisions puis fusions. On pourrait penser que le mécanisme de refroidissement évoqué plus haut devrait les aplatir mais celui -ci n'est valable que pour un fluide très lié où les échanges d'énergie sont fort, donc pour un gaz, mais pas pour un nuage d'étoiles qui s'influence directement très peu l'une avec l'autre (les libres parcours moyens, temps de relaxation, taux de collision sont bien éloignés de ceux d'un gaz ou même des étoiles d'un amas globulaire) ; on peut considérer que les étoiles d'une galaxie ne sont pas en interaction les unes avec les autres, elles ne voient que le champ global de la galaxie et ainsi il n'y a pas de refroidissement notable du point de vue de l'agitation cinétique des étoiles. 
 Non, toutes les galaxies ne sont "plates". Tu te feras rapidement une idée en consultant la séquence de Hubble --> https://fr.wikipedia.org/wiki/S%C3%A9que... 

Quant aux systèmes planétaires (autres que le notre), c'est une autre paire de manches. La discipline exoplanétaire est récente. Déjà que simplement classifier (définir des types (exo)planétaires) est encore à l'étude, pour ce qui est de la géométrie précise des systèmes planétaires, cela est probablement un tantinet prématuré --> https://fr.wikipedia.org/wiki/Exoplan%C3... 
Je prends le risque d'avancer que non, il doit probablement exister d'autres types de systèmes (avec les étoiles doubles par exemple ou autres loufoqueries) mais il est permis de conjecturer que beaucoup le sont (par mouvement de rotation).
   
Les systèmes solaires se forment à partir d'accumulations locales de matière sous forme de gaz. Le centre qui va devenir le noyau attire la matière environnante pour former un "globule de Bok". 
https://www.futura-sciences.com/sciences/definitions/univers-globules-bok-4670/ 

Il est initialement sphéroïde mais forcément pas homogène, ce qui va créer un axe de rotation privilégié lors des accrétions autour du quel va se trouver un maximum de matière, se trouvant donc sur un disque. 

Les nodules de matière hors de ce disque subissent une attraction gravitationnelle moins forte [moins de matière centrale] et donc soit seront ramenés dans le disque du fait de chocs, soit échapperont à l'attraction du système : ne subsistera en gros finalement qu’un disque approximatif. 
C'est pourquoi la forme de disque semble générale dans les systèmes solaires, hors histoires de chocs "récents" ou d'astres errants capturés. 

Pour les galaxies, on peut faire le même raisonnement à des échelles bien plus grandes. 
Mais leur histoire étant plus chaotique (il y a eu des rencontres et même des galaxies qui se sont traversées) les formes sont plus variées.
 Je crois savoir que dans notre système solaire, il y a au moins une planète qui n est pas dans le même écliptique que les autres, mais je ne me rappelle plus laquelle, pour ce qui est des autres systèmes, il y a beaucoup de diversité, il y en a même avec deux soleil par exemple...
 Quelques réponses. 
https://www.youtube.com/watch?v=HHgeR9OL...
 Sur un systéme observé rare ....c 'était l'étoile qui tournait autour de la planéte .